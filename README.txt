
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Peter Clarke <petermclarke@gmail.com>

Mythtv Api Server REQUIRES Services 7.x-3.x <http://drupal.org/project/services>

Mythtv Api Server is a wrapper/proxy with added user access control to the 
web services provided by the Mythtv Services Api <http://www.mythtv.org/wiki/Services_API>.

The reason to use it is to keep the Mythtv server behind a firewall on the local network
and expose just the service functions you require on a world accessable web service.
Some of the Mythtv Services Api functions are very powerfull and can be effectively disabled 
by using this service.


INSTALLATION
------------

1. Enable Mythtv Api Server

2. Add a new endpoint at /admin/structure/services/add
 - "Name" (eg; Mythtvapi)
 - "Server" select 'Mythtv Api Server'
 - "Path to endpoint" (eg; mythtvapi)

3. "Edit resources"; each of Mythtv's services are listed, with their functions.
Tick the checkbox of each function that you want to expose.
 
4. Configure your Mythtv backend details for you endpoint under the "Server" tab.

