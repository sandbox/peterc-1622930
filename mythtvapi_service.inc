<?php
/**
 * @file
 * Settings and service callbacks.
 */

/**
 * Server settings form.
 *
 * Generates the form fragment for configuring the server for an endpoint.
 *
 * @param array $form
 *   The form fragment array from services that define the settings.
 * @param object $endpoint
 *   The endpoint object that we're configuring the server for.
 * @param array $settings
 *   The current settings.
 */
function mythtvapi_service_settings(&$form, $endpoint, $settings) {
  $form['backend_api_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Api backend url'),
    '#required'      => TRUE,
    '#description'   => t('Example: http://BackendServerIP:6544'),
    '#default_value' => isset($settings['backend_api_url']) ? $settings['backend_api_url'] : '',
  );
}

/**
 * Submit handler for the services REST server settings form.
 *
 * @param object $endpoint
 *   The endpoint that's being configured.
 * @param array $values
 *   The partial form-state from services.
 *
 * @return array
 *   The settings for the REST server in this endpoint.
 */
function mythtvapi_service_settings_submit($endpoint, $values) {
  $values['backend_api_url'] = trim($values['backend_api_url'], '/');
  return $values;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function mythtvapi_service_form_services_edit_form_endpoint_server_alter(&$form, &$form_state, $form_id) {
  $form['#validate'][] = 'mythtvapi_service_services_edit_form_endpoint_server_validate';
}

/**
 * Validation for the server endpoint settings form.
 */
function mythtvapi_service_services_edit_form_endpoint_server_validate($form, &$form_state) {
  $backend_api_url = $form_state['values']['mythtvapi_service']['backend_api_url'];
  // Check for a reasonable url
  $parsed_url = parse_url($backend_api_url);

  if (empty($parsed_url['host'])) {
    form_set_error('', t('Invalid Api backend url.'));
  }

  if (empty($parsed_url['scheme'])) {
    // default to http:// when no scheme provided
    $form_state['values']['mythtvapi_service']['backend_api_url'] = 'http://' . $backend_api_url;
  }

}

/**
 * Makes the call to the Mythtv Services Api.
 *
 * @param array $settings
 *   An array of server settings as set in the server config page
 * @param string $service
 *   The name of the service to call as a string
 * @paramstring  $function
 *   The api function to call as a string
 * @param array $args
 *   An array of key value pairs for the function arguments
 * @param string $accept
 *   The accept header for formating the response format as a string
 */
function mythtvapi_service_action($settings, $service, $function, $args, $accept) {
  global $base_url;

  // Handle wsdl requests
  switch (drupal_strtolower($function)) {
    case 'wsdl':
    case 'xsd':
      $response = MythtvApiServer::callMythtv(
        $settings['backend_api_url'],
        'GET',
        $service,
        $function,
        $args += array('raw' => 1),
        $accept
      );
      return str_replace(
        $settings['backend_api_url'] . '/',
        $base_url . '/' . $settings['endpoint'] . '/',
        $response
      );
  }

  // Pass the request on to MythTv and stream the response back to the calling application
  MythtvApiServer::callMythtv(
    $settings['backend_api_url'],
    'POST',
    $service,
    $function,
    $args,
    $accept,
    TRUE
  );

}
