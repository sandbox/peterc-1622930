<?php
/**
 * @file
 * Class for handling server calls.
 */

class MythtvApiServer {

  private $endpoint;

  /**
   * Handles the call to the server
   *
   * @param string $canonical_path
   * @param string $endpoint_path
   * @return void
   */
  public function handle($canonical_path, $endpoint_path) {
    $this->endpoint_path = $endpoint_path;

    // Determine the request method
    $method = $_SERVER['REQUEST_METHOD'];

    // Prepare $path array and $resource_name.
    $path = explode('/', $canonical_path);
    $resource_name = array_shift($path);

    $endpoint = services_get_server_info('endpoint', '');
    $endpoint_definition = services_endpoint_load($endpoint);
    $this->endpoint = $endpoint_definition;
    // Get the server settings from the endpoint.
    $this->settings = !empty($endpoint_definition->server_settings) ? $endpoint_definition->server_settings : array();
    if (empty($this->settings['backend_api_url'])) {
      return services_error(t('Backend api url not configured.'), 501);
    }
    $this->settings['endpoint'] = $endpoint;

    $resources = services_get_resources($endpoint);

    $controller = FALSE;
    if (!empty($resource_name) && isset($resources[$resource_name])) {
      $resource = $resources[$resource_name];

      // Get the operation and fill with default values
      $controller = $this->resolveController($resource, $method, $path, $endpoint_definition, $resource_name);
    }

    if (!$controller) {
      return services_error(t('Could not find the controller.'), 404);
    }

    $options = array();

    // Prepare the arguments for the controller
    $arguments = $this->getControllerArguments($resource_name, $controller, $path, $method);

    try {
      $result = services_controller_execute($controller, $arguments, $options);
      $mime_type = $this->getAcceptHeader();
      // Set the content type and return the result
      drupal_add_http_header('Content-type', $mime_type);
      return $result;
    }
    catch (ServicesException $e) {
      return $this->handleException($e);

    }



  }

  /**
   * Calls the Mythtv Services Api
   */
  public static function callMythtv($backend_api_url, $method, $service_name, $function, $params = array(), $accept = 'text/xml', $stream = FALSE) {

    $url = $backend_api_url . '/' . $service_name . '/' . $function;
    if (count($params) > 0) {
      $url .=  '?';
      foreach ($params as $key => $value) {
        $url .= '&' . $key . '=' . $value;
      }
    }

    $context_opts = array(
      'http' => array(
        'method'  => $method,
        'timeout' => 10.5,
        'ignore_errors' => TRUE,
      )
    );
    $context = stream_context_create($context_opts);

    $options = array(
      'method' => $method,
      'timeout' => 10.5,
      'context' => $context,
      'headers' => array(
        'Accept' => $accept,
        'User-Agent' => 'MythtvApiClient',
      )
    );

    if (!empty($_SERVER['HTTP_SOAPACTION'])) {
      $options['headers']['SOAPACTION'] = $_SERVER['HTTP_SOAPACTION'];
      $options['headers']['CONTENT_TYPE'] = $_SERVER['CONTENT_TYPE'];
      $url = $backend_api_url . '/' . $service_name;
      $postdata = file_get_contents("php://input");
      $options['data'] = $postdata;
      $stream = TRUE;
    }

    if ($stream) {
      self::stream_http($url, $options);
    }
    else {

      $result = drupal_http_request($url, $options);

      if ($result->code == '200') {
        return $result->data;
      }

      if (isset($result->error)) {
        $msg = $result->error;
      }
      elseif (isset($result->status_message)) {
        $msg = $result->status_message;
      }
      else {
        $msg = 'Unkown error';
      }
      return MythtvApiServer::formatError($msg, $result->code);
    }
  }

  public static function stream_http($url, $options = array()) {
    drupal_session_commit();
    if (ob_get_level()) {
      ob_end_clean();
    }

    // Take a large chunk of code from drupal_http_request() without timeout...

    // Parse the URL and make sure we can handle the schema.
    $uri = @parse_url($url);

    //timer_start(__FUNCTION__);

    // Merge the default options.
    $options += array(
      'headers' => array(),
      'method' => 'POST',
      'data' => NULL,
      'max_redirects' => 3,
      'timeout' => 30.0,
      'context' => NULL,
    );
    // stream_socket_client() requires timeout to be a float.
    $options['timeout'] = (float) $options['timeout'];

    $port = isset($uri['port']) ? $uri['port'] : 80;
    $socket = 'tcp://' . $uri['host'] . ':' . $port;
    // RFC 2616: "non-standard ports MUST, default ports MAY be included".
    // We don't add the standard port to prevent from breaking rewrite rules
    // checking the host that do not take into account the port number.
    $options['headers']['Host'] = $uri['host'] . ($port != 80 ? ':' . $port : '');


    if (empty($options['context'])) {
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout']);
    }
    else {
      // Create a stream with context. Allows verification of a SSL certificate.
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout'], STREAM_CLIENT_CONNECT, $options['context']);
    }

    if ($fp) {

      // Construct the path to act on.
      $path = isset($uri['path']) ? $uri['path'] : '/';
      if (isset($uri['query'])) {
        $path .= '?' . $uri['query'];
      }

      // Only add Content-Length if we actually have any content or if it is a POST
      // or PUT request. Some non-standard servers get confused by Content-Length in
      // at least HEAD/GET requests, and Squid always requires Content-Length in
      // POST/PUT requests.
      $content_length = strlen($options['data']);
      if ($content_length > 0 || $options['method'] == 'POST' || $options['method'] == 'PUT') {
        $options['headers']['Content-Length'] = $content_length;
      }

      $request = $options['method'] . ' ' . $path . " HTTP/1.0\r\n";
      foreach ($options['headers'] as $name => $value) {
        $request .= $name . ': ' . trim($value) . "\r\n";
      }
      $request .= "\r\n" . $options['data'];

      fwrite($fp, $request);

      // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
      // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
      // instead must invoke stream_get_meta_data() each iteration.
      $info = stream_get_meta_data($fp);
      $alive = !$info['eof'] && !$info['timed_out'];

      // End of big copy & paste from drupal_http_request()

      while ($alive) {

        $chunk = fread($fp, 1024);

        // reset the timeout for php execution
        set_time_limit(30);

        if (!empty($header_end)) {
          // send on to the calling application
          print $chunk;
        }
        else {
          if (!isset($buffer)) {
            $buffer = '';
          }
          $buffer .= $chunk;
          $header_end = strpos($buffer, "\r\n\r\n");
          if ($header_end) {
            // Get the headers
            $header = substr($buffer, 0, $header_end);

            // Remove ALL headers queued for sending
            $headers_list = headers_list();
            foreach ($headers_list as $headers_item) {
              $parts = explode(':', $headers_item);
              header_remove($parts[0]);
            }

            // Set the headers as received
            $headers = self::http_parse_headers($header);
            foreach ($headers as $name => $value) {
              header($name . ': ' . $value);
            }

            // Send the rest of the chunk that is not part of the headers
            print substr($buffer, $header_end + 4);
            unset($buffer);
          }
        }

        $info = stream_get_meta_data($fp);
        $alive = !$info['eof'] && !$info['timed_out'] && $chunk;
      }
      fclose($fp);
      exit;
    }
    // TODO: handle $fp error

  }

  public static function http_parse_headers($header) {
    $ret = array();
    $fields = explode("\r\n", preg_replace('/\x0D\x0A[6\x09\x20]+/', ' ', $header));
    foreach( $fields as $i => $field ) {
      if ($i == 0) {
        //$ret['status'] = $field;
      }
      if( preg_match('/([^:]+): (.+)/m', $field, $match) ) {
        $match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
        if( isset($retVal[$match[1]]) ) {
          $ret[$match[1]] = array($ret[$match[1]], $match[2]);
        } else {
          $ret[$match[1]] = trim($match[2]);
        }
      }
    }
    return $ret;
  }

  public static function formatError($error, $error_code = NULL) {

    // The error should be in the format requested,
    // BUT a bug in Mythtv Services Api means errors are always reported as xml
    // so do the same until fixed.
    /*
    $accept = $this->getAcceptHeader();
    switch ($accept) {
      case 'application/json':
      case 'text/javascript':
        $e = new stdClass();
        $e->errorDescription = $error;
        $e->errorCode = $error_code;
        return json_encode($e);
        break;
    }
    */

    // xml
    // format the xml the same as mythtv services api
    return '<detail><errorDescription><![CDATA[ ' . $error . ' ]]></errorDescription><errorCode>' . $error_code . '</errorCode></detail>';
  }

  private function resolveController($resource, $method, $path, $endpoint, $resource_name) {

    if (!empty($_SERVER['HTTP_SOAPACTION'])) {
      $operation = trim(str_replace('http://mythtv.org/' . $resource_name . '/', '', $_SERVER['HTTP_SOAPACTION']), '"');
    }
    elseif (count($path) > 0) {
      $operation = $path[0];
    }
    else {
      // No fuction call provided
      return FALSE;
    }

    // We only deal with actions
    $class = 'actions';

    if (drupal_strtolower($operation) == 'xsd') {
      // automatically add xsd resources if wsdl is enabled
      if (!empty($resource['settings'][$class]['wsdl']['enabled'])) {
        $resource[$class]['xsd'] = $resource[$class]['wsdl'];
        $resource['settings'][$class]['xsd']['enabled'] = $resource['settings'][$class]['wsdl']['enabled'];
      }
    }

    // check the action is enabled
    if (empty($resource['settings'][$class][$operation]['enabled'])) {
      return FALSE;
    }

    $controller = FALSE;
    if (!empty($resource[$class][$operation])) {
      $controller = $resource[$class][$operation];
      if (isset($resource['file']) && empty($controller['file'])) {
        $controller['file'] = $resource['file'];
      }

    }
    // services_request_apply_version($controller, array('method' => $operation, 'resource' => $resource_name));
    return $controller;
  }

  /**
   * Parses controller arguments from request
   *
   * @param string $resource_name
   *  The name of the called resource
   * @param array $controller
   *  The controller definition
   * @param string $path
   *  The path to the controller
   * @param string $method
   *  The method used to make the request
   *
   * @return
   *  An array of arguments to be passed to the service callback.
   */
  private function getControllerArguments($resource_name, $controller, $path, $method) {

    $arguments = array();

    if (!empty($_SERVER['HTTP_SOAPACTION'])) {
      $xml = file_get_contents('php://input');
      $simplexml = simplexml_load_string($xml);
      $ns = $simplexml->getNamespaces(true);
      $soap = $simplexml->children($ns['SOAP-ENV']);
      $response = $soap->Body->children($ns['ns1']);
      $function = $response->getName();
      foreach ($response->children($ns['ns1']) as $key => $value) {
        $arguments[$key] = (string) $value;
      }
    }
    else {
      $function = $path[0];
      // Just forward all arguments on to the api
      foreach ($_GET as $key => $value) {
        // Remove Drupal's 'q'
        if ($key != 'q') {
          $arguments[$key] = $value;
        }
      }
    }

    $accept = $this->getAcceptHeader();
    // Pass the function call and the header accept to the function proxying the call
    return array('settings' => $this->settings, 'service' => $resource_name, 'function' => $function, 'args' => $arguments, 'accept' => $accept);
  }

  private function getAcceptHeader() {
    $accept = 'text/xml';
    if (isset($_SERVER['HTTP_ACCEPT'])) {
      $accept = $_SERVER['HTTP_ACCEPT'];
    }
    switch ($accept) {
      case 'application/json':
      case 'text/javascript':
        return $accept;
        break;
    }
    return $accept;
  }

  public function handleException($exception) {
    $code = $exception->getCode();
    switch ($code) {
      case 204:
        drupal_add_http_header('Status', '204 No Content: ' . $exception->getMessage());
        break;
      case 304:
        drupal_add_http_header('Status', '304 Not Modified: ' . $exception->getMessage());
        break;
      case 401:
        drupal_add_http_header('Status', '401 Unauthorized: ' . $exception->getMessage());
        break;
      case 404:
        drupal_add_http_header('Status', '404 Not found: ' . $exception->getMessage());
        break;
      case 406:
        drupal_add_http_header('Status', '406 Not Acceptable: ' . $exception->getMessage());
        break;
      default:
        if ($code >= 400 && $code < 600) {
          drupal_add_http_header('Status', $code . ':' . $exception->getMessage());
        }
        else {
          drupal_add_http_header('Status', '500 Internal Server Error: ' . 'An error occurred (' . $code . '):' . $exception->getMessage());
        }
        break;
    }
    if ($this->endpoint->debug) {
      debug($exception);
    }

    $result = $this->formatError($exception->getMessage(), $exception->getCode());
    $mime_type = 'text/xml'; // Currently Mythtv Services Api errors are xml even if json is requested
    // Set the content type and return the error message
    drupal_add_http_header('Content-type', $mime_type);
    return $result;

  }

}

